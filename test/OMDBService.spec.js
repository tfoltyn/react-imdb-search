import { expect } from 'chai';
import fetchMock from 'fetch-mock';
import { beforeEach } from 'mocha';
import OMDBService from '../src/service/OMDBService';
import Movie from '../src/model/Movie';

describe('OMDBService', () => {

  let service: OMDBService;
  const searchReponse: Array<any> = [
    { Title: 'One' },
    { Title: 'Two' },
  ];

  beforeEach(() => {
    service = new OMDBService();
    service.url = 'http://someurl';
  });

  it('should request external api for results', () => {
    // given
    fetchMock.get('*', { Search: searchReponse, Response: 'True' });
    // when
    service.search('query', 'type')
      .then(() => {
        // then
        expect(fetchMock.lastUrl()).to.equal('http://someurl?s=query&type=type');
        fetchMock.restore();
      });
  });

  it('should return array of Movies', () => {
    // given
    fetchMock.get('*', { Search: searchReponse, Response: 'True' });
    // when
    service.search('query', 'type')
      .then((result) => {
        // then
        expect(result).to.have.lengthOf(2);
        expect(result[1] instanceof Movie).to.be.true;
        fetchMock.restore();
      });
  });

  it('should return empty array if request does not provide any results', () => {
    // given
    fetchMock.get('*', { Search: searchReponse, Response: 'False' });
    // when
    service.search('query', 'type')
      .then((result) => {
        // then
        expect(result).to.have.lengthOf(0);
        fetchMock.restore();
      });
  });

});
