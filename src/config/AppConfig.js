export const APPLICATION_NAME = 'IMDB Movies';
export const MOVIE_POSTER_PLACEHOLDER = 'http://clickers.micromappers.org/static/img/placeholder.project.png';
export const OMDB_API_URL = 'http://www.omdbapi.com/';
export const IMDB_API_URL = 'http://www.imdb.com/title/';
export const SEARCH_CATEGORIES = ['movie', 'series', 'episode'];
export const SEARCH_HINTS_COUNT = 5;
export const SEARCH_HINT_TITLE_LENGHT = 20;
