import { SEARCH_HINT_TITLE_LENGHT } from '../config/AppConfig';

function trimTitle(value) {
  let result = value;
  if (result.length > SEARCH_HINT_TITLE_LENGHT) {
    result = value.slice(0, SEARCH_HINT_TITLE_LENGHT);
    result += '...';
  }
  return result;
}

class Movie {

  title;
  id;
  poster;
  year;

  constructor(title, id, poster, year, placeholder) {
    this.title = trimTitle(title);
    this.id = id;
    this.poster = (poster === 'N/A') ? placeholder : poster;
    this.year = year;
  }
}

export default Movie;
