import React from 'react';
import { MenuItem } from 'material-ui';

class Hint {
  text;
  value;
  id;

  menuItemStyle = {
    height: 60,
    width: 350,
  };

  imgStyle = {
    width: 30,
    height: 41,
    verticalAlign: 'middle',
  };

  constructor(model) {
    this.text = model.title;
    this.value = (
      <MenuItem
        style={this.menuItemStyle}
        leftIcon={<img style={this.imgStyle} src={model.poster} alt={model.title} />}
        primaryText={model.title}
        secondaryText={model.year}
      />
    );
    this.id = model.id;
  }
}

export default Hint;
