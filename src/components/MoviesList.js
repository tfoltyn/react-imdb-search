import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { goToMoviePage } from '../actions';
import MovieItem from './MovieItem';
import Movie from '../model/Movie';

class MoviesList extends React.Component {

  onItemClick = (item) => {
    this.props.goToMoviePage(item.id);
  };

  movies;

  divStyle = {
    marginTop: 30,
  };

  render() {
    this.movies = this.props.results.map(movie =>
      <MovieItem key={movie.id} model={movie} onClick={this.onItemClick} />,
    );
    return (<div style={this.divStyle}>{this.movies}</div>);
  }
}

MoviesList.propTypes = {
  goToMoviePage: PropTypes.func,
  results: PropTypes.arrayOf(PropTypes.instanceOf(Movie)),
};

const mapStateToProps = state => ({ results: state.results });
const mapDispatchToProps = () => ({
  goToMoviePage: (movieId) => {
    goToMoviePage(movieId);
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(MoviesList);
