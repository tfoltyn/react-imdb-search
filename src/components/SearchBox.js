import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { AutoComplete, RaisedButton, MenuItem, Paper } from 'material-ui';
import { blue400 } from 'material-ui/styles/colors';
import { SEARCH_CATEGORIES, SEARCH_HINTS_COUNT } from '../config/AppConfig';
import DropDownList from './DropDownList';
import { fetchHints, receiveHints, goToMoviePage, receiveResults } from '../actions';
import Hint from '../model/Hint';

class SearchBox extends React.Component {

  componentWillMount() {
    this.categories = SEARCH_CATEGORIES.map(category =>
      <MenuItem key={category} value={category} primaryText={category} />,
    );
  }

  onUpdateInput = (value) => {
    if (value !== '') {
      this.searchText = value;
      this.props.fetchHints(value, this.props.selectedCategory);
    }
  };

  onSearchButtonClick = () => {
    if (this.searchText !== '') {
      this.props.fetchResults(this.searchText, this.props.selectedCategory);
    }
  };

  onNewRequest = (item) => {
    if (item instanceof Hint) {
      this.props.goToMoviePage(item.id);
    } else {
      this.props.fetchResults(this.searchText, this.props.selectedCategory);
    }
  };

  searchText;
  isButtonSecondary = true;

  divStyle = {
    maxWidth: 700,
    marginLeft: 30,
    display: 'inline-block',
    position: 'relative',
    top: -5,
  };

  paperStyle = {
    verticalAlign: 'middle',
    display: 'inline-block',
    padding: '0px 10px',
    marginRight: 20,
    backgroundColor: blue400,
  };

  autoCompleteListStyle = {
    width: 350,
  };

  render() {
    return (
      <div style={this.divStyle}>
        <Paper style={this.paperStyle} zDepth={1}>
          <AutoComplete
            hintText="Search"
            dataSource={this.props.hints}
            listStyle={this.autoCompleteListStyle}
            maxSearchResults={SEARCH_HINTS_COUNT}
            onUpdateInput={this.onUpdateInput}
            onNewRequest={this.onNewRequest}
          />
        </Paper>
        <DropDownList />
        <RaisedButton label="Search" secondary={this.isButtonSecondary} onClick={this.onSearchButtonClick} />
      </div>
    );
  }
}

SearchBox.propTypes = {
  selectedCategory: PropTypes.string,
  hints: PropTypes.arrayOf(PropTypes.instanceOf(Hint)),
  fetchHints: PropTypes.func,
  fetchResults: PropTypes.func,
  goToMoviePage: PropTypes.func,
};

const mapStateToProps = state => ({
  selectedCategory: state.selectedCategory,
  hints: state.hints.map(hint => new Hint(hint)),
});

const mapDispatchToProps = dispatch => ({
  fetchHints: (query, type) => {
    fetchHints(query, type).then(hints => dispatch(receiveHints(hints)));
  },
  fetchResults: (query, type) => {
    fetchHints(query, type).then(results => dispatch(receiveResults(results)));
  },
  goToMoviePage: (movieId) => {
    goToMoviePage(movieId);
  },
});


export default connect(mapStateToProps, mapDispatchToProps)(SearchBox);
