import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { SelectField, Paper, MenuItem } from 'material-ui';
import { blue400 } from 'material-ui/styles/colors';
import { SEARCH_CATEGORIES } from '../config/AppConfig';
import { setNewCategory } from '../actions';

class DropDownList extends React.Component {

  componentWillMount() {
    this.categories = SEARCH_CATEGORIES.map(category =>
      <MenuItem key={category} value={category} primaryText={category} />,
    );
  }

  onCategoryChange = (event, index, value) => { this.props.changeCategory(value); };

  categories;

  paperStyle = {
    verticalAlign: 'middle',
    display: 'inline-block',
    padding: '0px 10px',
    marginRight: 20,
    backgroundColor: blue400,
    height: 48,
  };

  selectFieldStyle = {
    width: 120,
  };

  render() {
    return (
      <Paper style={this.paperStyle} zDepth={1}>
        <SelectField
          value={this.props.selectedCategory}
          style={this.selectFieldStyle}
          onChange={this.onCategoryChange}
        >
          {this.categories}
        </SelectField>
      </Paper>
    );
  }
}

DropDownList.propTypes = {
  changeCategory: PropTypes.func,
  selectedCategory: PropTypes.string,
};


const mapStateToProps = state => ({ selectedCategory: state.selectedCategory });
const mapDispatchToProps = dispatch => ({
  changeCategory: (category) => {
    dispatch(setNewCategory(category));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(DropDownList);
