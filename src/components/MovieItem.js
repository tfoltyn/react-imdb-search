import React from 'react';
import PropTypes from 'prop-types';
import { Paper, RaisedButton } from 'material-ui';
import { blue300 } from 'material-ui/styles/colors';
import Movie from '../model/Movie';

class MovieItem extends React.Component {

  onClick = () => { this.props.onClick(this.props.model); };

  isButtonSecondary = true;

  paperStyle = {
    padding: '10px 10px',
    backgroundColor: blue300,
    maxWidth: 1000,
    margin: 'auto',
    marginBottom: 10,
  };

  imgStyle = {
    width: 80,
    height: 108,
    display: 'inline-block',
    verticalAlign: 'middle',
    borderRadius: 5,
    border: 'solid 3px',
    borderColor: '#fff',
  };

  divStyle = {
    display: 'inline-block',
    verticalAlign: 'middle',
    marginLeft: 20,
  };

  titleStyle = {
    color: '#fff',
    cursor: 'pointer',
    marginTop: 10,
  };

  render() {
    return (
      <Paper style={this.paperStyle}>
        <img style={this.imgStyle} src={this.props.model.poster} alt={this.props.model.title} />
        <div style={this.divStyle}>
          <h4 style={this.titleStyle}>Title: {this.props.model.title}</h4>
          <RaisedButton label="visit page" secondary={this.isButtonSecondary} onClick={this.onClick} />
        </div>
      </Paper>
    );
  }
}

MovieItem.propTypes = {
  model: PropTypes.instanceOf(Movie),
  onClick: PropTypes.func,
};

export default MovieItem;
