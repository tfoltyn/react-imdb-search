import React from 'react';
import { APPLICATION_NAME } from '../config/AppConfig';

class Brand extends React.Component {

  hStyle = {
    marginLeft: 30,
    marginRight: 30,
    display: 'inline-block',
    color: '#fff',
  };

  render() {
    return (<h4 style={this.hStyle}>{APPLICATION_NAME}</h4>);
  }
}

export default Brand;
