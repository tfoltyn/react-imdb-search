import OMDBService from './service/OMDBService';
import IMDBService from './service/IMDBService';

export const CATEGORY = 'CATEGORY';
export const HINTS = 'HINTS';
export const RESULTS = 'RESULTS';

export function setNewCategory(category) {
  return { type: CATEGORY, payload: category };
}

export function receiveHints(hints) {
  return { type: HINTS, payload: hints };
}

export function receiveResults(hints) {
  return { type: RESULTS, payload: hints };
}

export function fetchHints(query, type) {
  return new OMDBService().search(query, type);
}

export function goToMoviePage(movieId) {
  new IMDBService().goToMoviePage(movieId);
}

