import { createStore } from 'redux';
import { SEARCH_CATEGORIES } from './config/AppConfig';
import { CATEGORY, HINTS, RESULTS } from './actions';

const initialState = {
  selectedCategory: SEARCH_CATEGORIES[0],
  hints: [],
  results: [],
};

const store = createStore((state = initialState, action) => {
  switch (action.type) {
    case CATEGORY:
      return { ...state, selectedCategory: action.payload };
    case HINTS:
      return { ...state, hints: action.payload };
    case RESULTS:
      return { ...state, results: action.payload };
    default:
      return state;
  }
});

export default store;
