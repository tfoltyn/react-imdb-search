import React from 'react';
import { Paper } from 'material-ui';
import { blue500 } from 'material-ui/styles/colors';
import Brand from '../components/Brand';
import SearchBox from '../components/SearchBox';
import MoviesList from '../components/MoviesList';

class HomePage extends React.Component {

  paperStyle = {
    backgroundColor: blue500,
    width: '100%',
  };

  render() {
    return (
      <div>
        <Paper style={this.paperStyle}>
          <Brand />
          <SearchBox />
        </Paper>
        <MoviesList />
      </div>
    );
  }
}

export default HomePage;
