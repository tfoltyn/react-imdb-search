import { OMDB_API_URL, MOVIE_POSTER_PLACEHOLDER } from '../config/AppConfig';
import Movie from '../model/Movie';

class OMDBService {

  url;

  constructor() {
    this.url = OMDB_API_URL;
  }

  search(query, type) {
    return fetch(`${this.url}?s=${query}&type=${type}`)
      .then(response => response.json())
      .then(response => ((response.Response === 'False') ? [] : response.Search))
      .then(movies => movies.map(movie =>
        new Movie(movie.Title, movie.imdbID, movie.Poster, movie.Year, MOVIE_POSTER_PLACEHOLDER),
      ));
  }
}

export default OMDBService;
