import { IMDB_API_URL } from '../config/AppConfig';

class IMDBService {

  url;

  constructor() {
    this.url = IMDB_API_URL;
  }

  goToMoviePage(movieId) {
    window.location.href = `${this.url}${movieId}/`;
  }
}

export default IMDBService;
